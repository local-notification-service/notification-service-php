<?php

namespace App\Controller;

use App\Service\NotificationService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Attribute\Route;

class NotifcationController extends AbstractController
{
    private NotificationService $service;

    public function __construct(NotificationService $notificationService)
    {
        $this->service = $notificationService;
    }

    #[Route('/notification', name: 'get_notification', methods:"GET")]
    public function get(Request $request): JsonResponse
    {
        try{
            $result = $this->service->get($request->get("data") ?? []);
        } catch (\Exception $e) {
            $result = ["error" => $e->getMessage()];
        }

        return $this->json($result);
    }

    #[Route('/notification', name: 'save_notification', methods:"POST")]
    public function save(Request $request): JsonResponse
    {
        try{
            $result = $this->service->save($request->get("data") ?? []);
        } catch (\Exception $e) {
            $result = ["error" => $e->getMessage()];
        }

        return $this->json($result);
    }

    #[Route('/notification', name: 'delete_notification', methods:"DELETE")]
    public function delete(Request $request): JsonResponse
    {
        try{
            $result = $this->service->delete($request->get("data") ?? []);
        } catch (\Exception $e) {
            $result = ["error" => $e->getMessage()];
        }

        return $this->json($result);
    }
}
