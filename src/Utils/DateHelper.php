<?php

namespace App\Utils;

use DateTime;

class DateHelper
{
    public static function formatDate(string|null $date): string|null
    {
        if(empty($date)) return null;
        $dt = new DateTime($date);
        $dt->modify("+3 hours");
        return $dt->format("Y-m-d H:i:s");
    }
}