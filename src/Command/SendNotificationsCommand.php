<?php

namespace App\Command;

use App\Entity\Notification;
use App\Utils\DateHelper;
use Doctrine\ORM\EntityManagerInterface;
use Predis\Client;
use Predis\ClientInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'notify:send',
    description: 'Get and throw to queue notifications by date',
)]
class SendNotificationsCommand extends Command
{

    private EntityManagerInterface $em;
    private ClientInterface $redis;

    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->em = $entityManagerInterface;
        $this->redis = new Client("tcp://nsredis:6379");
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        foreach($this->em->getRepository(Notification::class)->findNotificationsToSend() as &$notification) {
            if(empty($notification['id']) || empty($notification['text'])) continue;
            $obj = $this->em->getRepository(Notification::class)->find($notification['id']);
            if($obj->getSent() < 1 ||
            (!empty($notification['date_retry']) && $obj->getSent() < 2 && DateHelper::formatDate(date("Y-m-d H:i:s")) >= $notification['date_retry']->format("Y-m-d H:i:s"))
            ) {
                $this->redis->lpush("php-service-queue",[$obj->getText()]);
                $obj->markAsSent();
            }
            if(empty($notification['date_retry']) || DateHelper::formatDate(date("Y-m-d H:i:s")) >= $notification['date_retry']->format("Y-m-d H:i:s")) $this->em->remove($obj);
            unset($notification);
        }

        $this->em->flush();
        return Command::SUCCESS;
    }
}
