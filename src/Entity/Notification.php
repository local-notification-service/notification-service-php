<?php

namespace App\Entity;

use App\Repository\NotificationRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\IdGenerator\UuidGenerator;
use Symfony\Component\Uid\Factory\UuidFactory;

#[ORM\Entity(repositoryClass: NotificationRepository::class)]
class Notification
{
    #[ORM\Id]
    #[ORM\Column(type: Types::GUID)]
    #[ORM\GeneratedValue(strategy:"CUSTOM")]
    #[ORM\CustomIdGenerator(class:UuidGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 255)]
    private ?string $text = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $parentData = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private ?\DateTimeInterface $date = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTimeInterface $date_retry = null;

    #[ORM\Column(type: Types::INTEGER, nullable: true)]
    private ?int $sent = 0;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): static
    {
        $this->text = $text;

        return $this;
    }
    
    public function getParentData(): ?string
    {
        return $this->parentData;
    }

    public function setParentData(string $parentData): static
    {
        $this->parentData = $parentData;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): static
    {
        $this->date = $date;

        return $this;
    }

    public function getDateRetry(): ?\DateTimeInterface
    {
        return $this->date_retry;
    }

    public function setDateRetry(\DateTimeInterface $date_retry): static
    {
        $this->date_retry = $date_retry;

        return $this;
    }

    public function getSent(): ?int
    {
        return $this->sent;
    }

    public function markAsSent(): ?static
    {
        $this->sent = !empty($this->sent) ? $this->sent++ : 1;
        return $this;
    }
}
