<?php

namespace App\Repository;

use App\Entity\Notification;
use App\Utils\DateHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Notification>
 */
class NotificationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Notification::class);
    }

    public function findNotificationsToSend(): array|null
    {
        $now = DateHelper::formatDate(date("Y-m-d H:i:s"));
        return $this->getEntityManager()->createQueryBuilder()->select("n")->from("App\Entity\Notification","n")
            ->where("n.date <= '{$now}'")
                ->orWhere("n.date_retry <= '{$now}'")
                    ->getQuery()
                        ->getArrayResult() 
                            ?? [];
    }

    public function deleteByParent(string $parent): void 
    {
            $this->getEntityManager()->createQueryBuilder()->delete("App\Entity\Notification","n")->where("n.parent_data = {$parent}")->getQuery()->execute();
    }
}
