<?php

namespace App\Service;

use App\Entity\Notification;
use App\Repository\NotificationRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class NotificationService 
{

    private EntityManagerInterface $em;
    private NotificationRepository $repo;

    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->em = $entityManagerInterface;
        $this->repo = $this->em->getRepository(Notification::class);
    }

    public function get(array $data): array|null
    {
        return $this->repo->findBy($data);
    }

    public function getOne(array $data): array|null
    {
        return $this->repo->findOneBy($data);
    }

    public function findToSend(): array|null
    {
        return $this->repo->findNotificationsToSend();
    }

    public function save(array $data): Notification
    {
        if(!empty($data['id'])) {
            $model = $this->repo->find($data['id']);
        } else {
            $model = new Notification();
        }

        if(!empty($data['text'])) $model->setText($data['text']);
        if(!empty($data['parent_data'])) $model->setText($data['parent_data']);

        if(!empty($data['date'])) $model->setDate(new DateTime($data['date']));
        if(!empty($data['date_retry'])) $model->setDateRetry(new DateTime($data['date_retry']));

        $this->em->persist($model);
        $this->em->flush();

        return $model;
    }

    public function delete(array $data): void
    {
        if(!empty($data['id'])) {
            $this->em->remove($this->repo->find($data['id']));
            $this->em->flush();
        } else if (!empty($data['parent_data'])) {
            $this->repo->deleteByParent($data['parent_data']);
        }
    }
}